'use strict';

var Parser = require('../src/parser').Parser;
var FA = require('../src/fa').FA;
var Joi = require('joi');

module.exports = function(server) {
    
    server.route({
        method: 'POST',
        path: '/api/parse',
        config: {
            tags: ['api'],
            validate: {
                payload: {
                    code: Joi.string().required()
                }
            },
            handler: function (request, reply) {

                try{
                    var parser;
                    parser = new Parser();
                    parser.initParsing(request.payload.code);

                    return reply(parser.run());
                }catch(err){
                    var obj = {status: "Error", message: err.message};
                    return reply(obj);
                }
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/stateEliminator',
        config: {
            tags: ['api'],
            validate: {
                payload: {
                    dfa_struct: Joi.string().required(),
                    states_eliminate: Joi.string().required(),
                    final_state: Joi.string().required()
                }
            },
            handler: function (req, reply) {

                try{
                    var fa = new FA();
                    var dfa = JSON.parse(req.payload.dfa_struct);
                    var statesE = JSON.parse(req.payload.states_eliminate);
                    fa.init(dfa);
                    fa._stateEliminator(statesE, req.payload.final_state);

                    return reply(fa);
                }catch(err){
                    var obj = {status: "Error", message: err.message};
                    return reply(obj);
                }
            }
        }
    });

    // Serve static files
    server.route({
        method: 'GET',
        path: '/{name*}',
        handler: {
            directory: {
                path: '../Client',
                redirectToSlash: true,
                index: true
        }
    }
    });
};