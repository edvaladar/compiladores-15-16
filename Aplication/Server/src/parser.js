var Lexer = require('./lexer').Lexer;
var TOKEN_TYPE = require('./lexer').TOKEN_TYPE;

/*
 * DOT GRAMMAR
 *
 *  The following is  an abstract grammar  deﬁning the DOT language. Terminals are 
 *  shown in bold font and nonterminals in  italics. Literal characters  are given 
 *  in single  quotes. Parentheses ( and ) indicate  grouping  when needed. Square 
 *  brackets [ and ] enclose optional items. Vertical bars | separate alternatives.
 *
 *
 * 
 *  graph : [ strict ] (graph | digraph) [ ID ] ’{’ stmt_list ’}’         **DONE**
 *  stmt_list : [ stmt [ ’; ’ ] stmt_list ] 							  **DONE**
 *  stmt : node_stmt | edge_stmt | attr_stmt | ID ’=’ ID | subgraph       **DONE**
 *  attr_stmt : (graph | node | edge) attr_list                           **DONE**
 *  attr_list : ’[ ’ [ a_list ] ’] ’ [ attr_list ]                        **DONE**
 *  a_list : ID ’=’ ID [ ( ’; ’ | ’ , ’) ] [ a_list ]                     **DONE**
 *  edge_stmt : (node_id | subgraph) edgeRHS [ attr_list ]                **DONE**
 *  edgeRHS : edgeop (node_id | subgraph) [ edgeRHS ]                     **DONE**
 *  node_stmt : node_id [ attr_list ]                                     **DONE**
 *  node_id : ID [ port ] 												  **DONE**
 *  port : ’: ’ ID [ ’: ’ compass_pt ] | ’: ’ compass_pt             **NOT IMPLEMENTED**
 *  subgraph : [ subgraph [ ID ] ] ’{’ stmt_list ’}’                 **NOT IMPLEMENTED**
 *  compass_pt : (n | ne | e | se | s | sw | w | nw | c | _)         **NOT IMPLEMENTED**
 *
 */

function Parser() {
   this.grammar_index = 0;  
   this.displayRunningTimeInfo =  true;   

   this.TOKEN_ARRAY = [];           
   this.TOKENS_DIVERSITY = {}; 

   this.messages = [];

   this.parsed_tree = {};
   this.dfa_struct = {
		title: "",
		start_states: [],
		final_states: [],
		transitions:[]
	};        

	this.current_master_node = "";                   
};

function Parse_Error(message){
	return new Error(message);
}

var push_child = function(obj, element){
	if(element != null)
		obj.children.push(element);

}
Parser.prototype._graph = function() { // graph : [ strict ] (graph | digraph) [ ID ] ’{’ stmt_list ’}’  **DONE**

	var graf_obj = {
		node: "graph",
		children: []
	};

	// [ strict ]         (optative)
	push_child(
		graf_obj,
		this._checkToken(TOKEN_TYPE.STRICT, {optative: true, error_message: ""})
	);

	// (graph | digraph)  Verifies the existence of one of the two primitives.
	push_child(
		graf_obj,
		this._checkToken2(TOKEN_TYPE.GRAPH , TOKEN_TYPE.DIGRAPH, {optative: false, error_message: "\nThe graph or digraph primitive is missing."})
	);

	// [ ID ]             (optative)
	push_child(
		graf_obj,
		this._checkID(true)
	);

	this.dfa_struct.title = graf_obj.children[graf_obj.children.length - 1].leaf.text;
	//graf_obj.children.push({"leaf": "ID"});

	// ’{’                Verifies the existence of a L_BRACK
	push_child(
		graf_obj,
		this._checkToken(TOKEN_TYPE.L_BRACK, {optative: false, error_message: "\nThe \'{\' primitive is missing."})
	);

	/* 
	 * Process the statement list (stmt_list) TOKENS
	 */
		push_child(graf_obj,this._stmt_list());


	// ’}’                Verifies the existence of a R_BRACK
	push_child(
		graf_obj,
		this._checkToken(TOKEN_TYPE.R_BRACK, {optative: false, error_message: "\nThe \'}\' primitive is missing."}) 
	);
	
	this.messages.push("\n**SUCCESS**\nDOT script code was sucessfuly parsed!");

	this.parsed_tree = graf_obj;
}

Parser.prototype._stmt_list = function() { // stmt_list : [ stmt [ ’; ’ ] stmt_list ]  **DONE**

		var stmt_list_obj = {
			node: "stmt_list",
			children: []
		};

		/* 
	 	 * Process the statement (stmt) TOKENS
		 */
	
			push_child(stmt_list_obj,this._stmt());

		// [ ’; ’ ]           (optative)
		push_child(
			stmt_list_obj,
			this._checkToken(TOKEN_TYPE.SEMICOLON, {optative: true, error_message: ""})
		); 

		/* 
	 	 * Process the statement list (stmt_list) TOKENS
	 	 */
			if(this.hasNext() && this.TOKEN_ARRAY[this.grammar_index].type != TOKEN_TYPE.R_BRACK)
				push_child(stmt_list_obj,this._stmt_list());

		return stmt_list_obj;
}

Parser.prototype._stmt = function() { // stmt : node_stmt | edge_stmt | attr_stmt | ID ’=’ ID | subgraph 

	var stmt_obj = {
		node: "stmt",
		children: []
	};

	/* 
	 * Process the node statement (_node_stmt) TOKENS
	 */
	if(this.hasNext() &&
		((this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.ID       && this.TOKEN_ARRAY[this.grammar_index+1].type != TOKEN_TYPE.EDGEOP && this.TOKEN_ARRAY[this.grammar_index+1].type != TOKEN_TYPE.EQUAL   ) ||
		(this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.QUOTE    && this.TOKEN_ARRAY[this.grammar_index+2].type != TOKEN_TYPE.EDGEOP && this.TOKEN_ARRAY[this.grammar_index+2].type != TOKEN_TYPE.EQUAL 
																		  && this.TOKEN_ARRAY[this.grammar_index+3].type != TOKEN_TYPE.EDGEOP && this.TOKEN_ARRAY[this.grammar_index+3].type != TOKEN_TYPE.EQUAL)))
			push_child(stmt_obj,this._node_stmt());
	else
	/* 
	 * Process the edge statement (_edge_stmt) TOKENS
	 */
	if(this.hasNext() &&
		((this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.ID     && this.TOKEN_ARRAY[this.grammar_index+1].type == TOKEN_TYPE.EDGEOP ) ||
		 (this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.QUOTE  && (this.TOKEN_ARRAY[this.grammar_index+2].type == TOKEN_TYPE.EDGEOP  || this.TOKEN_ARRAY[this.grammar_index+3].type == TOKEN_TYPE.EDGEOP))))
			push_child(stmt_obj,this._edge_stmt());
	else
	/* 
	 * Process the attribut statement (_attr_stmt) TOKENS
	 */
	if(this.hasNext() &&
		(this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.GRAPH  ||
		 this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.NODE   ||
		 this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.EDGE   ))
			push_child(stmt_obj,this._attr_stmt());
	else
	/* 
	 * Process the ID '=' ID
	 */
	if(this.hasNext() &&
		(this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.ID  || this.TOKEN_ARRAY[this.grammar_index+2].type == TOKEN_TYPE.QUOTE)){  //SOME LOOKAHEAD

		// ID                 Verifies the existence of an ID
		push_child(
			stmt_obj,
			this._checkID(false)
		); 
		// ’=’                Verifies the existence of an EQUAL
		push_child(
			stmt_obj,
			this._checkToken(TOKEN_TYPE.EQUAL, {optative: false, error_message: "\nThe \'=\' primitive is missing."})
		); 
		// ID                 Verifies the existence of an ID
		push_child(
			stmt_obj,
			this._checkID(false)
		);
	}
	else
		throw new Error('Parsing error. The input TOKENS are not valids to execute stmt function.');

	return stmt_obj;
}

Parser.prototype._attr_stmt = function() { // attr_stmt : (graph | node | edge) attr_list **DONE**
	
	var attr_stmt_obj = {
		node: "attr_stmt",
		children: []
	};
	
	// (graph | node | edge)        Verifies the existence of one of the three primitives.   
	push_child(
		attr_stmt_obj,
		this._checkToken3(TOKEN_TYPE.GRAPH, TOKEN_TYPE.NODE, TOKEN_TYPE.EDGE, {optative: true, error_message: ""})
	); 

	if(attr_stmt_obj.children[attr_stmt_obj.children.length - 1].leaf.text == 'node')
		this.current_master_node = "node";

	/* 
	 * Process the attribut list (_attr_list) TOKENS
	 */
		push_child(attr_stmt_obj,this._attr_list());

	return attr_stmt_obj;
}

Parser.prototype._attr_list = function(){ // attr_list : ’[ ’ [ a_list ] ’] ’ [ attr_list ] **DONE**

	var attr_list_obj = {
		node: "attr_list",
		children: []
	};

	// ’[’                Verifies the existence of a L_SQR_BRACK
	push_child(
		attr_list_obj,
		this._checkToken(TOKEN_TYPE.L_SQR_BRACK, {optative: false, error_message: "\nThe \'[\' primitive is missing."})
	); 

	/* 
	 * Process the attribut list (_a_list) TOKENS
	 */
	
		push_child(attr_list_obj,this._a_list());

	// ’]’                Verifies the existence of a R_SQR_BRACK
	push_child(
		attr_list_obj,
		this._checkToken(TOKEN_TYPE.R_SQR_BRACK, {optative: false, error_message: "\nThe \']\' primitive is missing."})
	); 

	if(this.hasNext() && this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.L_SQR_BRACK)
		push_child(attr_list_obj,this._attr_list());

	return attr_list_obj;
}

Parser.prototype._a_list = function(){ // a_list : ID ’=’ ID [ ( ’; ’ | ’ , ’) ] [ a_list ] **DONE**

	var a_list_obj = {
		node: "a_list",
		children: []
	};

	// ID                      Verifies the existence of a ID
	push_child(
		a_list_obj,
		this._checkID(false)
	);

	// ’=’                     Verifies the existence of a L_SQR_BRACK
	push_child(
		a_list_obj,
		this._checkToken(TOKEN_TYPE.EQUAL, {optative: false, error_message: "\nThe \'=\' primitive is missing."})
	); 

	// ID                      Verifies the existence of a ID
	push_child(
		a_list_obj,
		this._checkID(false)
	);

	if(a_list_obj.children[a_list_obj.children.length - 1].leaf.text == 'circle')
		this.current_master_node += "-circle";
	else
		if(a_list_obj.children[a_list_obj.children.length - 1].leaf.text == 'doublecircle')
			this.current_master_node += "-doublecircle";
		else
			if(this.current_master_node.val == "-")
				this.current_master_node.val = a_list_obj.children[a_list_obj.children.length - 1].leaf.text;


	// [ ( ’; ’ | ’ , ’) ]     (optative)
	push_child(
		a_list_obj,
		this._checkToken2(TOKEN_TYPE.SEMICOLON , TOKEN_TYPE.COMMA, {optative: true, error_message: ""})
	);

	if(this.hasNext() && (this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.ID || this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.QUOTE))
		push_child(a_list_obj,this._a_list());

	return a_list_obj;
}

Parser.prototype._node_stmt = function(){ // node_stmt : node_id [ attr_list ] **DONE**
	var node_stmt_obj = {
		node: "node_stmt",
		children: []
	};

	/* 
	 * Process the node id (_node_id) TOKENS
	 */
	
		push_child(node_stmt_obj,this._node_id());

	if(this.hasNext() && this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.L_SQR_BRACK)
		push_child(node_stmt_obj,this._attr_list());

	return node_stmt_obj;
}

Parser.prototype._node_id = function(){ // node_id : ID [ port ] **DONE**

	var node_id_obj = {
		node: "node_id",
		children: []
	};

	push_child(
		node_id_obj,
		this._checkID(true)
	);

	if(node_id_obj.children.length > 0)
	if(this.current_master_node == "node-circle"){
		this.dfa_struct.start_states.push(node_id_obj.children[node_id_obj.children.length - 1].leaf.text);
	}
	else
		if(this.current_master_node == "node-doublecircle"){
			this.dfa_struct.final_states.push(node_id_obj.children[node_id_obj.children.length - 1].leaf.text);
		}
		else
			if(this.current_master_node.src == "-")
				this.current_master_node.src = node_id_obj.children[node_id_obj.children.length - 1].leaf.text;
			else
				if(this.current_master_node.dst == "-")
					this.current_master_node.dst = node_id_obj.children[node_id_obj.children.length - 1].leaf.text;

	// port not used by now

	return node_id_obj;
}

Parser.prototype._edge_stmt = function(){ // edge_stmt : (node_id | subgraph) edgeRHS [ attr_list ]  **DONE**
	
	var edge_stmt_obj = {
		node: "edge_stmt",
		children: []
	};

	this.current_master_node = {
			src: "-",
			dst: "-",
			val: "-"
		};

	/* 
	 * Process the node id (_node_id) TOKENS
	 */
	push_child(edge_stmt_obj,this._node_id()); // or subgraph(not implemented)

	/* 
	 * Process the edgeRHS (_edgeRHS) TOKENS
	 */
	push_child(edge_stmt_obj,this._edgeRHS());

	if(this.hasNext() && this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.L_SQR_BRACK)
		push_child(edge_stmt_obj,this._attr_list());

	if(this.current_master_node.src != "-" && this.current_master_node.dst != "-" && this.current_master_node.val != "-")
		this.dfa_struct.transitions.push(this.current_master_node);

	this.current_master_node = "";

	return edge_stmt_obj;
}

Parser.prototype._edgeRHS = function(){ // edgeRHS : edgeop (node_id | subgraph) [ edgeRHS ]    **DONE**
	
	var edgeRHS_obj = {
		node: "edgeRHS",
		children: []
	};
	
	// EDGEOP                      Verifies the existence of a EDGEOP
	push_child(
		edgeRHS_obj,
		this._checkToken(TOKEN_TYPE.EDGEOP, {optative: false, error_message: ""})
	);

	/* 
	 * Process the node id (_node_id) TOKENS
	 */
	push_child(edgeRHS_obj,this._node_id()); // or subgraph(not implemented)

	if(this.hasNext() && this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.EDGEOP)
		push_child(edgeRHS_obj,this._edgeRHS());

	return edgeRHS_obj;
}







Parser.prototype._checkID = function(_optative){
	var return_value;
	if(this.hasNext() && this.TOKEN_ARRAY[this.grammar_index].type == TOKEN_TYPE.QUOTE){
		// "                      Verifies the existence of a "
		this._checkToken(TOKEN_TYPE.QUOTE, {optative: false, error_message: ""});

		// ID                      Verifies the existence of a ID
		return_value = this._checkToken(TOKEN_TYPE.ID, {optative: _optative, error_message: ""});

		// "                      Verifies the existence of a "
		this._checkToken(TOKEN_TYPE.QUOTE, {optative: false, error_message: "Missing \" to close the ID"});
	}
	else
		// ID                      Verifies the existence of a ID
		return_value =  this._checkToken(TOKEN_TYPE.ID, {optative: _optative, error_message: ""});

	return return_value;
}

Parser.prototype._checkToken = function(_TOKEN_TYPE, options){
	if(this.hasNext() &&
		this.TOKEN_ARRAY[this.grammar_index].type == _TOKEN_TYPE){
			this.grammar_index++;
			return {
				leaf: { 
					text: this.TOKEN_ARRAY[this.grammar_index-1].text,
					type: this.TOKEN_ARRAY[this.grammar_index-1].type
					}
				};
		}
	else
		if(!options.optative)
			throw new Error('*PARSING ERROR*\n' 
				+ 'Parser expected a ' + _TOKEN_TYPE + ' TOKEN, but the entrie is a ' + this.TOKEN_ARRAY[this.grammar_index].type + ' TOKEN.'
				+ options.error_message + '\n'
				+ 'Input TOKEN (' + this.TOKEN_ARRAY[this.grammar_index].type + ') number ' + (this.grammar_index+1) + '.\n');
}

Parser.prototype._checkToken2 = function(_TOKEN_TYPE1, _TOKEN_TYPE2, options){
	if(this.hasNext() &&
		this.TOKEN_ARRAY[this.grammar_index].type == _TOKEN_TYPE1 ||
		this.TOKEN_ARRAY[this.grammar_index].type == _TOKEN_TYPE2){
			this.grammar_index++;
			return {
				leaf: { 
					text: this.TOKEN_ARRAY[this.grammar_index-1].text,
					type: this.TOKEN_ARRAY[this.grammar_index-1].type
					}
				};
		}
	else
		if(!options.optative)
			throw new Error('*PARSING ERROR*\n' 
				+ 'Parser expected a ' + _TOKEN_TYPE1 + ' or ' + _TOKEN_TYPE2 + ' TOKEN, but the entrie is a ' + this.TOKEN_ARRAY[this.grammar_index].type + ' TOKEN.' 
				+  options.error_message + '\n'
				+ 'Input TOKEN (' + this.TOKEN_ARRAY[this.grammar_index].type + ') number ' + (this.grammar_index+1) + '.\n');
}

Parser.prototype._checkToken3 = function(_TOKEN_TYPE1, _TOKEN_TYPE2, _TOKEN_TYPE3, options){
	if(this.hasNext() &&
		this.TOKEN_ARRAY[this.grammar_index].type == _TOKEN_TYPE1 ||
		this.TOKEN_ARRAY[this.grammar_index].type == _TOKEN_TYPE2 ||
		this.TOKEN_ARRAY[this.grammar_index].type == _TOKEN_TYPE3){
			this.grammar_index++;
			return {
				leaf: { 
					text: this.TOKEN_ARRAY[this.grammar_index-1].text,
					type: this.TOKEN_ARRAY[this.grammar_index-1].type
					}
				};
		}
	else
		if(!options.optative)
			throw new Error('*PARSING ERROR*\n' 
				+ 'Parser expected a ' + _TOKEN_TYPE1 + ', ' + _TOKEN_TYPE2 + ' or ' + _TOKEN_TYPE3 + ' TOKEN, but the entrie is a ' + this.TOKEN_ARRAY[this.grammar_index].type + ' TOKEN.' 
				+  options.error_message + '\n'
				+ 'Input TOKEN (' + this.TOKEN_ARRAY[this.grammar_index].type + ') number ' + (this.grammar_index+1) + '.\n');
}

Parser.prototype.hasNext = function() {

	if (this.TOKEN_ARRAY){
		if(!(this.grammar_index < this.TOKEN_ARRAY.length))
			throw new Error('Parsing error. Primitives missing. No TOKENS to continue the analyse.');

	}
	else
		throw new Error('Parsing error. Primitives missing. No TOKENS to continue the analyse.');

	return true;
}

Parser.prototype.toOutputObject = function(){
	console.log("Tokens diversity: " + JSON.stringify(this.TOKENS_DIVERSITY));

	return {
		'lexical_analisys': {
			'n_tokens': this.grammar_index,
			'tokens': this.TOKEN_ARRAY,  
			'token_diversity': this.TOKENS_DIVERSITY
		},
   		'syntactic_analysis':{
   			'parsed_tree': this.parsed_tree,
   			'formated_tree': this.formatTree(),
   			'dfa_struct': this.dfa_struct,
   			'messages': this.messages
   		}
	}
}
 
Parser.prototype.formatTree = function() {

	var outputTree = {
		text:{
			name: this.parsed_tree.node
		},
		children: []
	};

	for(var i in this.parsed_tree.children){
		if(this.parsed_tree.children[i].leaf)
			outputTree.children.push(processLeaf(this.parsed_tree.children[i].leaf));
		else
			outputTree.children.push(processNode(this.parsed_tree.children[i]));
	}

	return outputTree;
}

processNode = function(_node) {

	var outputTree = {
		text:{
			name: _node.node
		},
		children: []
	};

	for(var i in _node.children){
		if(_node.children[i].leaf)
			outputTree.children.push(processLeaf(_node.children[i].leaf));
		else
			outputTree.children.push(processNode(_node.children[i]));
	}

	return outputTree;
}

processLeaf = function(_leaf) {

	return{
		text:{
			name: _leaf.type
		}
	};
}

Parser.prototype.initParsing = function(script_code) {

 	this.lexer = new Lexer(script_code);

	for(var itr in TOKEN_TYPE)
		this.TOKENS_DIVERSITY[TOKEN_TYPE[itr]] = 0;

	if(this.displayRunningTimeInfo)
		console.log('TOKENS Generation (Lexical Analysis)\n');

	
		var _temp_TOKEN;
		while (this.lexer.hasNext()){

			if(_temp_TOKEN = this.lexer.nextToken())
				this.TOKEN_ARRAY.push(_temp_TOKEN);
			else
				continue;

			if(this.displayRunningTimeInfo)
				console.log(this.TOKEN_ARRAY[this.TOKEN_ARRAY.length-1]);

			this.TOKENS_DIVERSITY[_temp_TOKEN.type] ++;
		}

	if(this.displayRunningTimeInfo){
		var orderlyTOKENS = '';

		for(var i in this.TOKEN_ARRAY)
			orderlyTOKENS += this.TOKEN_ARRAY[i].type + ' ';

		console.log('\nTOKENS in a row:\n\n' + orderlyTOKENS);
		console.log(this.TOKENS_DIVERSITY);
	}	
}

Parser.prototype.run = function(){
	this._graph();

	return this.toOutputObject();
}

module.exports.Parser = Parser;