function FA(){
	this.title;
	this.start_state = [];
	this.final_state = [];
	this.states = [];
	this.transitions = [];

	this.state_eliminator_steps = [];
} 

FA.prototype.init = function(struct){

	this.title = struct.dfa_title;
	this.start_state = struct.start_states;
	this.final_state = struct.final_states;

	if(this.start_state.length < 1){
		throw new Error("Semantic error. The DFA don't have a start state.");
	}

	if(this.final_state.length < 1){
		throw new Error("Semantic error. The DFA don't have a final state.");
	}

	for(var i in struct.start_state)
		this.addState(struct.start_state[i]);

	for(var i2 in struct.final_state)
		this.addState(struct.final_state[i2]);

	for(var i3 in struct.transitions)
		this.addTransition(struct.transitions[i3]);
} 



FA.prototype.addTransition = function(obj){

	this.addState(obj.src);
	this.addState(obj.dst);

	this.transitions.push(new Transition(obj.src, obj.dst, obj.val));
}

FA.prototype.updateTransition = function(state_src, state_dst, val){
	for(var i in this.transitions){
		if(this.transitions[i].source == state_src 
			&& this.transitions[i].destination == state_dst){
				this.transitions[i].value = val;
				return;
			}
	}
}

FA.prototype.removeTransition = function(state_src, state_dst){
	var i;
	for(i in this.transitions){
		if(this.transitions[i].source == state_src 
			&& this.transitions[i].destination == state_dst)
				break;
	}

	this.transitions.splice(i, 1);
}

FA.prototype.getTransitionVal = function(src_state, dst_state){
	for(var i in this.transitions){
		if(this.transitions[i].source == src_state 
			&& this.transitions[i].destination == dst_state)
			return this.transitions[i].value;
	}

	return "";
}

FA.prototype.existTransition_qf_q0 = function(){

	for(var i in this.final_state)
		if(this.getTransitionVal(this.final_state[i], this.start_state[0]) != "")
			return true;

	return false;
}




FA.prototype.addState = function(id){
	
	if(this.stateExist(id))
		return false;

	this.states.push(id);

	return true;
}

FA.prototype.removeState = function(state_id){

	var index_to_remove = this.states.indexOf(state_id);
	var new_transitions = [];

	this.states.splice(index_to_remove, 1);

	for(var i in this.transitions)
		if(!(this.transitions[i].source == state_id || this.transitions[i].destination == state_id))
			new_transitions.push(this.transitions[i]);

	this.transitions = new_transitions;
}

FA.prototype.stateExist = function(id){

	if(this.states.indexOf(id) != -1)
		return true;

	return false;
}

FA.prototype.getSources =  function(state_id){
	var output_states = [];

	for(var i in this.transitions){
		if(this.transitions[i].destination == state_id 
			&& this.transitions[i].source != state_id)
				output_states.push(this.transitions[i].source);
	}

	return output_states;
}

FA.prototype.getDestinations =  function(state_id){
	var output_states = [];

	for(var i in this.transitions){
		if(this.transitions[i].source == state_id 
			&& this.transitions[i].destination != state_id)
				output_states.push(this.transitions[i].destination);
	}

	return output_states;
}




FA.prototype.updateTransitions = function(Src, Dst, q){
	var val = "", si_q, q_q, q_dj;
	var transtitionExist = false;

	//RE(si→dj) + RE(si→q).(RE(q→q))*.RE(q→dj)
	if(val = this.getTransitionVal(Src, Dst)){
		val += "|";
		transtitionExist = true;
	}

	si_q = this.getTransitionVal(Src,q);
	q_q  = this.getTransitionVal(q,q); 
	q_dj = this.getTransitionVal(q, Dst);

	if(si_q != "")
			if(q_q != "")
				if(q_dj != "")
					val +=  si_q + 
					"("  + q_q  + ")*" +
					q_dj;
				else
					val +=  si_q + 
					"("  + q_q  + ")*";
			else
				if(q_dj != "")
					val +=  si_q + 
					q_dj;
				else
					val +=  si_q;
		else
			if(q_q != "")
				if(q_dj != "")
					val +=  "("  + q_q  + ")*" +
					q_dj;
				else
					val +=  "("  + q_q  + ")*";
			else
				if(q_dj != "")
					val +=  q_dj;
	

	if(transtitionExist)
		this.updateTransition(Src, Dst, val);
	else
		this.transitions.push(new Transition(Src, Dst, val));

	return val;
}

FA.prototype.eliminateState = function(state){
	var S = this.getSources(state);
	var D = this.getDestinations(state);

	var str = "";

	for(var si in S)
		for(var dj in D)
			str += this.updateTransitions(S[si], D[dj], state);

	this.removeState(state);

	this.state_eliminator_steps.push({
		fase: "Elimination Fase",
		stateToRemove: state,
		script: this.toDotScript()
	});

	return str;
}

FA.prototype.setFinalState = function(state){

	this.final_state.splice(this.final_state.indexOf(state), 1);
	this.final_state.unshift(state);

	for(var i in this.final_state){

		var current_state = this.final_state[i];

		if(current_state != state){

			var tranVal = this.getTransitionVal(this.start_state[0], this.final_state[0]) + "|" +
						  this.getTransitionVal(this.start_state[0], current_state);

			if(this.getTransitionVal(this.final_state[0], current_state) != "")
				tranVal += "|" + this.getTransitionVal(this.start_state[0], this.final_state[0]) + this.getTransitionVal(this.final_state[0], current_state);

			if(this.getTransitionVal(current_state, this.final_state[0]) != "")
				tranVal += "|" + this.getTransitionVal(this.start_state[0], current_state) + this.getTransitionVal(current_state, this.final_state[0]);

			this.updateTransition(this.start_state[0], this.final_state[0], tranVal);

			this.removeState(current_state);

			//this.final_state.splice(this.final_state.indexOf(current_state), 1);
			
			this.state_eliminator_steps.push({
				fase: "Final State Elimination Fase",
				stateToRemove: current_state,
				script: this.toDotScript()
			});
		}
	}

	this.final_state = [state];
}

FA.prototype._stateEliminator = function(states_to_remove, final_state){
    var queue = states_to_remove;

    this.state_eliminator_steps.push({
    		fase: "Start Fase",
    		script: this.toDotScript()
    	});

    while(queue.length > 0)
    	this.eliminateState(queue.shift());

    var qf_q0, q0_qf,
    	q0_q0, qf_qf,
    	re = "";

    q0_q0 = this.getTransitionVal(this.start_state[0], this.start_state[0]);

    if(q0_qf != "" && this.final_state.length+this.start_state.length == this.states.length){

    	if(true){
    		for(var itr in this.final_state){
    			qf_q0 = this.getTransitionVal(this.final_state[itr], this.start_state[0]);
    			q0_qf = this.getTransitionVal(this.start_state[0], this.final_state[itr]);

    			if(qf_q0 != ""){
    		
		    		qf_qf = this.getTransitionVal(this.final_state[itr], this.final_state[itr]);

					if(qf_qf == ""){
						var temp = "";

						if(q0_q0 != "")
							temp = qf_q0 + "(" + q0_q0 + ")*" + q0_qf;
						else
							temp = qf_q0 + q0_qf;

						this.transitions.push(new Transition(this.final_state[itr], this.final_state[itr], temp));
					}
					else{
						var temp = "";

						if(q0_q0 != "")
							temp = qf_q0 + "(" + q0_q0 + ")*" + q0_qf;
						else
							temp = qf_q0 + q0_qf;

						this.updateTransition(this.final_state[itr], this.final_state[itr], qf_qf + "|" + temp);
					}

		    		this.removeTransition(this.final_state[itr], this.start_state[0]);

		    		

		    		this.state_eliminator_steps.push({
		    			fase: "Transition From qf to q0 removing",
		    			script: this.toDotScript()
		    		});
		    	}

		    	qf_qf = this.getTransitionVal(this.final_state[itr], this.final_state[itr]);

		    	if(qf_qf != ""){
		    		this.removeTransition(this.final_state[itr], this.final_state[itr]);

		    		this.updateTransition(this.start_state[0], this.final_state[itr], this.getTransitionVal(this.start_state[0], this.final_state[itr]) + "(" + qf_qf + ")*");

		    		this.state_eliminator_steps.push({
    					fase: "Final State Cicle Removing Fase",
    					script: this.toDotScript()
    				});
		    	}
    		}
    	}

    	if(this.final_state.length > 1)
    		this.setFinalState(final_state);

    	

    	q0_qf = this.getTransitionVal(this.start_state[0], this.final_state[0]);
    	qf_qf = this.getTransitionVal(this.final_state[0], this.final_state[0]);

    	if(q0_q0 || qf_qf){

    		if(q0_q0 != ""){
    			re += "(" + q0_q0 + ")*";
    			this.removeTransition(this.start_state[0], this.start_state[0]);
    		}

    		re += "(" + q0_qf + ")";

    		this.updateTransition(this.start_state[0], this.final_state[0], re);

    	

    		this.state_eliminator_steps.push({
    			fase: "Cicle Removing Fase",
    			script: this.toDotScript()
    		});
    	}

    }

    /*
   5. while Queue Empty do 
	 1. State q = Queue.get(); 
	 2. Set S = q.getSources() – {q}; 
	 3. Set D = q.getDestinations() – {q}; 
	 4. Remove q from FA but keep info about transitions to/from q; 
	 5. foreachpair (si, dj) | si S dj D do 
	 	1. Add a transition si → dj if it does not exist; 
	 	2. Add a regular expression to transition si→dj: RE(si→dj) + RE(si→q).(RE(q→q))*.RE(q→dj); // passing through q 
	 6. endforeach 
   6. dowhile 
   7. if exists qf → q0 then 
   		1. remove qf → q0 and add qf → qf with the RE: RE(qf →q0).RE(q0→qf); 
   8. endif 
   9. RE = (RE(q0→q0))*.RE(q0→qf).(RE(qf→qf))*   */
    		

}


FA.prototype.toDotScript = function(){
	var start_states_str = this.start_state.toString().replace(/,/g,"; ");
	var final_state_str = this.final_state.toString().replace(/,/g,"; ");

	var script = "digraph " + this.title + "{\
		rankdir = LR;\
  		node [shape = circle]; " + start_states_str + ";\
  		node [shape = doublecircle]; " + final_state_str + ";\
  		node [shape = plaintext];\
  		\"\" -> " + this.start_state[0] + " [label = \"start\"];\
  		node [shape = circle];";

  		for(var i in this.transitions)
  			script += this.transitions[i].source + "->" + this.transitions[i].destination + " [label=\"" + this.transitions[i].value + "\"];";

  	script += "}";

  	return script;
}


function Transition(src, dst, value){
	this.source = src;
	this.destination = dst;
	this.value = value;
}

module.exports.FA = FA;