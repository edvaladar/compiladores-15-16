/**
 * @class
 * @example
 * function Token(type, text) {
 *     this.type = type;
 *     this.text = text;
 * }
 * @param {TOKEN_TYPE} type  
 * @param {String} text 
 * @description This contructer receive a TOKEN_TYPE and a String with the token value as input
 */
function Token(type, text) {
  /**
    * @type {object}
    * @property {TOKEN_TYPE} type Token type.
    */
  this.type = type;
  /**
    * @type {object}
    * @property {String} text Token string value.
    */
  this.text = text;
}

/**
 * @class
 * @example 
 * function Lexer(regString) {
 *     this.regString = regString;                           
 *     this.index = 0;                                                           
 * };
 *
 * @param {String} regString Dotscript code as a string.
 * @description This constructer receive a String as input and
   construct a Lexer object.
 */
function Lexer(regString) {

  /**
    * @type {object}
    * @property {String} regString Dotscript code as a string.
    */
  this.regString = regString;   

  /**
    * @type {object}
    * @property {Number} index Position of the current charater to analyse.
    */                    
  this.index = 0;                                                           
};

/**
 * @description
 * TOKEN_TYPE is an object that contains all types of TOKENS that can 
 * be consumed in the lexer analysis.
 * @memberof Lexer
 * @static
 */
const TOKEN_TYPE = {
  ID:             'ID',
  L_BRACK:        'L_BRACK',
  R_BRACK:        'R_BRACK',
  L_SQR_BRACK:    'L_SQR_BRACK',
  R_SQR_BRACK:    'R_SQR_BRACK',
  EQUAL:          'EQUAL',
  END:            'EOF',
  SEMICOLON:      'SEMICOLON',
  COMMA:          'COMMA',
  HTML_L_BRACK:   'HTML_L_BRACK',
  HTML_R_BRACK:   'HTML_R_BRACK',
  QUOTE:          'QUOTE',
  STRICT:         'STRICT',         /////
  SUBGRAPH:       'SUBGRAPH',       /////
  GRAPH:          'GRAPH',          /////    RESERVED
  DIGRAPH:        'DIGRAPH',        /////    WORDS
  NODE:           'NODE',           /////     
  EDGE:           'EDGE',           /////
  EDGEOP:         'EDGEOP'          /////
};

/**
 * @example 
 * function hasNext() {
 *     if (this.regString)
 *         return this.index < this.regString.length;
 *     return false;
 * }
 * @return {Boolean} This method returns true if has a next character, otherwise, return false.
 * @description  The hasNext() method verify if the current Lexer index is lower than the regString length.
 */
Lexer.prototype.hasNext = function() {
  if (this.regString)
    return this.index < this.regString.length;
  return false;
}


/**
 * @example 
 * function Token(type, text) {
 *     this.type = type;
 *     this.text = text;
 *}
 * @return {Token} This method returns a Token object if has next token and if next token is valid, otherwise, return null;
 * @description The nextToken() method identify the 
   next Token  in the  regString, consume  the
   next character  of  the regString  and verify if this char
   belongs to  the  alphabet or if is the start of a reserved
   word or ID (in this cases consume more than one character).
 */
Lexer.prototype.nextToken = function() {
  if (this.hasNext()) {

    if(this.regString[this.index] == ' '  || this.regString[this.index] == '\n' || 
       this.regString[this.index] == '\t' || this.regString[this.index] == '\r' ||
       this.regString[this.index] == '\\' || this.regString[this.index] == '\w'){
        
        ++this.index;
        return null;
    }

    switch (this.regString[this.index]) {
      case 'd':
        return this.generateReservedToken(TOKEN_TYPE.DIGRAPH, 'digraph');
      case 'e':
        return this.generateReservedToken(TOKEN_TYPE.EDGE, 'edge');
      case 'g':
        return this.generateReservedToken(TOKEN_TYPE.GRAPH, 'graph');
      case 'n':
        return this.generateReservedToken(TOKEN_TYPE.NODE, 'node');
      case 's':
        return this.generateReservedToken(TOKEN_TYPE.STRICT, 'strict');
      case '-':
        return this.generateReservedToken(TOKEN_TYPE.EDGEOP, '->');
      case '<':
        return this.generateToken(TOKEN_TYPE.HTML_L_BRACK, '<');
      case '>':
        return this.generateToken(TOKEN_TYPE.HTML_R_BRACK, '>');
      case '{':
        return this.generateToken(TOKEN_TYPE.L_BRACK, '{');
      case '}':
        return this.generateToken(TOKEN_TYPE.R_BRACK, '}');
      case '[':
        return this.generateToken(TOKEN_TYPE.L_SQR_BRACK, '[');
      case ']':
        return this.generateToken(TOKEN_TYPE.R_SQR_BRACK, ']');
      case '=':
        return this.generateToken(TOKEN_TYPE.EQUAL, '=');
      case ';':
        return this.generateToken(TOKEN_TYPE.SEMICOLON, ';');
      case ',':
        return this.generateToken(TOKEN_TYPE.COMMA, ',');
      case '"':
        return this.generateToken(TOKEN_TYPE.QUOTE, '"');
      default:
        if(isDotID(this.regString[this.index]))
          return this.generateIDToken();
        else
          throw new Error('*LEXER ERROR*\n'
            +'Lexer get an unknown character as input \'' + this.regString[this.index] + '\'');
   }
 }
 else
  return "There's no TOKENS to consume.";
}


/**
 * @example 
 * function generateToken(TOKEN_TYPE, token){
 *     ++this.index;
 *     return new Token(TOKEN_TYPE, token);
 * }
 *  
 * @param {TOKEN_TYPE} TOKEN_TYPE      
 * @param {Token} token  
 * @return {Token}  This method returns a Token object.
 * @description The generateToken() method receive
   as input a TOKEN_TYPE and a String with the content of
   the token. The inputs are used to generate a Token and 
   the Lexer.index is incremented.   
 */
Lexer.prototype.generateToken = function(TOKEN_TYPE, token){
    ++this.index;
    return new Token(TOKEN_TYPE, token);
}

/**
 * @example
 *
 *  function generateReservedToken(TOKEN_TYPE, token){
 *
 *      if (this.hasNext() && this.regString[this.index+(token.length-1)] != null) {
 *          var _temp_str = this.regString.substring(this.index, this.index+token.length);
 *  
 *          if(_temp_str === token && !isDotID(this.regString[this.index+token.length])){
 *              this._consume(token.length);
 *              return new Token(TOKEN_TYPE, token);
 *          }
 *          else
 *              if(_temp_str === token && !isDotID(this.regString[this.index+(token.length-1)])){
 *                  this._consume(token.length);
 *                  return new Token(TOKEN_TYPE, token);
 *              }
 *              else
 *                  return this.generateIDToken();
 *          }
 *      else
 *          return this.generateIDToken();
 *  } 
 *
 * @param {TOKEN_TYPE} TOKEN_TYPE
 * @param {String} token      
 * 
 * @return     {Token} This method returns a Token object.
 * @description The generateReservedToken() method receive as  input a TOKEN_TYPE and a  String with the 
   content of the token. Analyse the next
   characters of the regString based in the token.length,
   by comparing the String between the index and index +
   + token.lenght with token content. If  match is a re-
   served TOKEN otherwise can be an ID TOKEN. In case of
   a reserved TOKEN the Lexer.index  is incremented with 
   the input token lenght.
 */
Lexer.prototype.generateReservedToken = function(TOKEN_TYPE, token){
  if (this.hasNext() && this.regString[this.index+(token.length-1)] != null) {
    var _temp_str = this.regString.substring(this.index, this.index+token.length);
    
    if(_temp_str === token && !isDotID(this.regString[this.index+token.length])){
      this._consume(token.length);
      return new Token(TOKEN_TYPE, token);
    }
    else
      if(_temp_str === token && !isDotID(this.regString[this.index+(token.length-1)])){
      this._consume(token.length);
      return new Token(TOKEN_TYPE, token);
    }
    else
      return this.generateIDToken();
  }
  else
      return this.generateIDToken();
} 
 
/**
 * 
 * @example
 *  function generateIDToken(){
 *    var _temp = '';
 *    
 *    while(this.hasNext()){
 *      if(isDotID(this.regString[this.index])){
 *        _temp = _temp + this.regString[this.index];
 *        ++this.index;
 *      }
 *      else
 *        break;
 *    }
 *    return new Token(TOKEN_TYPE.ID, _temp);
 *  }
 * @return     {Token} This method returns a Token object.
 * @description The generateIDToken() method iterate for the next  
   characters in regString, if
   Lexer.hasNext(), and stop when  find a  character 
   tha don't belong to the ID character set. In each
   iteration the Lexer.index is incremented.
 */
Lexer.prototype.generateIDToken = function(){
  var _temp = '';
      
  while(this.hasNext()){
    if(isDotID(this.regString[this.index])){
      _temp = _temp + this.regString[this.index];
      ++this.index;
    }
    else
      break;
  }
  return new Token(TOKEN_TYPE.ID, _temp);
}

/**
 *  
 * @example
 *  function _consume(n) {
 *    return this.index+= n;
 *  }
 * @param {number} n Return the incremented Index.
 * @description The _consume() method receive as  
   input an  Unsigned Integer that represent the  number 
   of increments. The Lexer.index is incremented n times.
 */
Lexer.prototype._consume = function(n) {
  return this.index+= n;
}

/**
 *  
 * @param {Char} regChar 
 * @return  {Boolean} This method returns true if the regChar is acceptable, otherwise, return false.
 * @description The isDotID() method receive a char as input and
   returns TRUE if the character is an valid ID otherwise 
   returns FALSE.
 */
function isDotID(regChar) {
  return (regChar >= 'a'    && regChar <= 'z')    || //char
         (regChar >= 'A'    && regChar <= 'Z')    || //char
         (regChar >= '\200' && regChar <= '\377') || //char
         (regChar >= '0' && regChar <= '9')       || //digit
          regChar == '_' || regChar == 'ε' ||
          regChar == '(' || regChar == ')' ||
          regChar == '*' || regChar == '•' ||
          regChar == '+' || regChar == '.';  //special char
}

function Lexer_Error(message){
  return new Error({'type': 'Lexer_Error', 'message': message});
}

module.exports.Lexer = Lexer;
module.exports.TOKEN_TYPE = TOKEN_TYPE;