angular.module('starter.controllers', [])

    .controller('MainCtrl', function ($scope, parseService) {

        $scope.data = {
            script_example: null
        };

        $scope.tempScripts = {
            ex1:   'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    2->5 [label="c"];\n\
    3->4 [label="e"];\n\
}',
            ex2:   'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    2->5 [label="c"];\n\
    3->2 [label="d"];\n\
    3->4 [label="e"];\n\
}',
            ex3: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    2->5 [label="c"];\n\
    3->2 [label="d"];\n\
    3->3 [label="e"];\n\
}',
            ex4: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 3; 4; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    2->4 [label="c"];\n\
    2->5 [label="d"];\n\
}',
            ex5: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 4; 5; 6;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    3->2 [label="c"];\n\
    3->4 [label="b"];\n\
    2->5 [label="d"];\n\
    2->6 [label="e"];\n\
}',
            ex6: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 4; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    3->4 [label="c"];\n\
    2->5 [label="d"];\n\
    4->4 [label="e"];\n\
    1->1 [label="f"];\n\
    5->5 [label="g"];\n\
}',
            ex7: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 4; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    3->4 [label="c"];\n\
    4->3 [label="d"];\n\
    2->5 [label="e"];\n\
    4->4 [label="f"];\n\
    1->1 [label="g"];\n\
    5->5 [label="h"];\n\
}',
            ex8: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 4; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    3->4 [label="c"];\n\
    2->5 [label="d"];\n\
    5->1 [label="e"];\n\
}',
            ex9: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 4; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    2->3 [label="b"];\n\
    3->4 [label="c"];\n\
    2->5 [label="d"];\n\
    5->1 [label="e"];\n\
    4->1 [label="f"];\n\
}',
            ex10: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 4; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    1->1 [label="b"];\n\
    2->3 [label="c"];\n\
    3->4 [label="d"];\n\
    2->5 [label="e"];\n\
    5->1 [label="f"];\n\
    5->5 [label="g"];\n\
    4->1 [label="h"];\n\
    4->4 [label="i"];\n\
}',
            ex11: 'digraph finite_state_machine {\n\
    rankdir = LR;\n\
    node [shape = circle]; 1;\n\
    node [shape = doublecircle]; 3; 5;\n\
    node [shape = plaintext];\n\
    "" -> 1 [label = "start"];\n\
    node [shape = circle];\n\
    1->2 [label="a"];\n\
    1->1 [label="g"];\n\
    2->3 [label="b"];\n\
    2->5 [label="c"];\n\
    3->2 [label="d"];\n\
    5->5 [label="h"];\n\
    5->1 [label="i"];\n\
}'
        };
        $scope.parsedObject = "";
        $scope.nextPage = "View Structures";

        $scope.statesToRemove = [];
        $scope.finalState = {val: "", obj: null};
        $scope.removeColor = 25;

        $scope.cleanOutputs = function(){
            $scope.statesToRemove = [];
            $scope.parsedObject = "";
            $scope.finalState = {val: "", obj: null};

            $('section#content2 p').empty();
            $('section#content1 div#tokens_info div.log').empty();
            $('section#content1 div#tokens div.log').empty();
            $('div.dfa_display').empty();
            $('div.elimination_steps_display').empty();
            $('#OrganiseChart-simple').empty();
            $('section#content2 #parser_info .object').empty();
            $('section#content2 #parser_info .running_log').empty();

            $scope.removeColor = 25;
        }

        $scope.addStateToRemove = function(state_id){
            var i = $scope.statesToRemove.indexOf(state_id);

            if(i != -1){
                $scope.statesToRemove.splice(i, 1);
                return 'removed';
            }
            else{
                $scope.statesToRemove.push(state_id);
                return 'added';
            }
        }

        $scope.updateNextPage = function(){
            if($scope.nextPage === "View Structures")
                $scope.nextPage = "View Logs"
            else
                if($scope.nextPage === "View Logs")
                    $scope.nextPage = "View Structures";
        }

        $scope.eliminateStates = function(){

            if(!$scope.parsedObject){
                console.log("There is no Parsed object.");
                return;
            }

            if($scope.finalState.val == ""){
                console.log("There is no final state selected.");
                return;
            }

            parseService.EliminateStates(JSON.stringify($scope.parsedObject.syntactic_analysis.dfa_struct), JSON.stringify($scope.statesToRemove), $scope.finalState.val)
            .then(function (result) {

                if(result.status == "Error"){
                    $('input#tab2').prop('checked', true);
                    alert(result.message);
                    return;
                }

                console.log(result);

                $('div.elimination_steps_display').empty();
                var alreadySet = false;

                for(var i in result.state_eliminator_steps){
                    var temp = result.state_eliminator_steps[i].script;
                    temp = Viz(temp, 'svg', 'dot');

                    $('div.elimination_steps_display').append('<p></p>');
                    if(result.state_eliminator_steps[i].fase == "Elimination Fase" && alreadySet)
                        $('div.elimination_steps_display p:last-child').append('<h2>Step ' + i +' - State ' + result.state_eliminator_steps[i].stateToRemove +' removed</h2>');
                    else{
                        if(result.state_eliminator_steps[i].fase == "Elimination Fase"){
                            alreadySet = true;

                            $('div.elimination_steps_display p:last-child').append('<h1>' + result.state_eliminator_steps[i].fase + '</h1> ');
                            $('div.elimination_steps_display p:last-child').append('<h2>Step ' + i +' - State ' + result.state_eliminator_steps[i].stateToRemove +' removed</h2>');
                        }
                        else{
                            $('div.elimination_steps_display p:last-child').append('<h1>' + result.state_eliminator_steps[i].fase + '</h1> ');
                            $('div.elimination_steps_display p:last-child').append('<h2>Step ' + i +'</h2>');
                        }

                        
                    }
                    $('div.elimination_steps_display p:last-child').append(temp);
                }
            });
        }

        $scope.parse = function(){

            $scope.cleanOutputs();

			var dot_script = $("#dot_script").val();

            parseService.Parse(dot_script)
            .then(function (result) {
                if(result.status == "Error"){
                    $('input#tab2').prop('checked', true);
                    $('section#content2 #parser_info .running_log').append(result.message);
                    return;
                }
                console.log(result);
                $scope.parsedObject = result;

                let tokens = $scope.parsedObject.lexical_analisys.tokens;
                let tokens_info = $scope.parsedObject.lexical_analisys.token_diversity;

                for(let itr2 in tokens_info){
                    $('section#content1 div#tokens_info div.log').append('<div class="token_use">\
                                                                    <span class="token_type">'+itr2+'</span>\
                                                                    <span class="n_uses">'+tokens_info[itr2]+'</span>\
                                                                </div>');
                }

                for(let itr in tokens){
                    $('section#content1 div#tokens div.log').append('<div class="token">\
                                <span class="green">Token</span> { \
                                <span class="purple">type</span>: \
                                <span class="light-orange">'+tokens[itr].type+'</span>,\
                                <span class="purple">text</span>: \
                                <span class="light-orange">'+tokens[itr].text+'</span> }\
                            </div>');
                }
                var str = JSON.stringify($scope.parsedObject.syntactic_analysis.parsed_tree, undefined, 4);
                $('section#content2 #parser_info .object').append(str);

                for(let itr3 in $scope.parsedObject.syntactic_analysis.messages)
                    $('section#content2 #parser_info .running_log').append($scope.parsedObject.syntactic_analysis.messages[itr3]);
                

                //output(str);
                var nfa = Viz(dot_script, 'svg', 'dot');
                $('div.dfa_display').append(nfa);

                $('text').css('cursor', 'pointer');

                $('g text').on('click', function(){

                    var node_val = $( this ).text();
                    var final_states = $scope.parsedObject.syntactic_analysis.dfa_struct.final_states;

                    if($scope.parsedObject.syntactic_analysis.dfa_struct.start_states.indexOf(node_val) != -1)
                        return;
                    else
                        if(final_states.indexOf(node_val) != -1){
                            
                            if($scope.finalState.val != "")
                                $scope.finalState.obj.attr( "fill", "#FFFFFF" );

                            $( $( this ).parent().find('ellipse').get(0) ).attr( "fill", "#00FF00" );

                            $scope.finalState = {val: node_val, obj: $( $( this ).parent().find('ellipse').get(0) )};

                            console.log($scope.finalState);
                        }
                        else{
                            var ret = $scope.addStateToRemove(node_val);

                            $scope.removeColor += 5;

                            if(ret == 'removed')
                                $( this ).parent().find('ellipse').attr( "fill", "#FFFFFF" );
                            else
                                $( this ).parent().find('ellipse').attr( "fill", "hsl(4, 90%, " + $scope.removeColor +"%)" );


                            console.log($scope.statesToRemove);
                        }
                    
                    
                });

                var simple_chart_config = {
                    chart: {
                        container: "#OrganiseChart-simple",
                        connectors: {
                            type: 'straight'
                        }
                    },
                    nodeStructure: $scope.parsedObject.syntactic_analysis.formated_tree
                };

                new Treant( simple_chart_config );
            });
		};
    });

