'use strict';

app.service('parseService', ['ServerConfig', '$q', '$http',
        function (ServerConfig, $q, $http) {

            this.Parse = function (script_code) {
                var deferred = $q.defer();

                $http.post('/api/parse', {
                    code: script_code
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

            this.EliminateStates = function (dfa_structure, states_to_eliminate, _final_state) {
                var deferred = $q.defer();

                $http.post('/api/stateEliminator', {
                    dfa_struct: dfa_structure,
                    states_eliminate: states_to_eliminate,
                    final_state: _final_state
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };
        }]
);
